require 'test_helper'

describe Customer do

	let(:sarah) {customers(:sarah)}
	
	describe 'validations' do
		it "is a valid customer" do
			expect(sarah).must_be :valid?
		end
	end

end